# Embeddable components for Sangha

[![Published on webcomponents.org](https://img.shields.io/badge/webcomponents.org-published-blue.svg)](https://www.webcomponents.org/element/sangha/sangha-embed)

Collection of embeddable standalone components for Sangha.

## Components

### Donation form
```html
<script src="https://embed.techcultivation.org/bower_components/webcomponentsjs/webcomponents-loader.js"></script>
<link rel="import" href="https://embed.techcultivation.org/sangha-donation-form.html">

<sangha-donation-form
  api-url="https://api.sangha.techcultivation.org/v1"
  project-ids='[1]'
  buttons='[500,1000,2000]'
  default-amount='500'>

  <!-- non JS Fallback -->
  <h3>Enable Javascript for interactive donation process</h3>
  <ul>
    <li>Donate to the following address with the following reference</li>
    <li>Static paypal button</li>
    <li>Link to donation receipt</li>
    <li>Mail address for help</li>
  </ul>
</sangha-donation-form>
```

### Contributor images
```html
<script src="https://embed.techcultivation.org/bower_components/webcomponentsjs/webcomponents-loader.js"></script>
<link rel="import" href="https://embed.techcultivation.org/sangha-project-contributors.html">

<sangha-project-contributors
  api-url="https://api.sangha.techcultivation.org/v1"
  project-id='1'
></sangha-project-contributors>
```

### Top projects
```html
<script src="https://embed.techcultivation.org/bower_components/webcomponentsjs/webcomponents-loader.js"></script>
<link rel="import" href="https://embed.techcultivation.org/sangha-top-projects.html">

<sangha-top-projects
  api-url="https://api.sangha.techcultivation.org/v1"
  limit="4"
></sangha-top-projects>
```

## Development

```bash
# Get dependencies
$ npm install

# Demo site
$ npm start

# Run tests
$ npm test
```
